import React, { useContext } from "react";
import ThemeContext from "../context/ThemeContext";
import AppTheme from "../theme/colours";

const headerStyles = {
    padding: "1rem",
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center"
};
const Header = () => {

  const [theme, setTheme] = useContext(ThemeContext);
  const currentTheme = AppTheme[theme];

  function changeTheme() {
    setTheme(theme === 'light' ? 'dark' : 'light');
  }

  return(
      <header style = {{...headerStyles, backgroundColor: currentTheme.backgroundColor}}>
          <h1>Context API</h1>
          <br/>
          <input type="button" onClick={changeTheme} value="Change Theme" />
      </header>
  );
}

export default Header;
