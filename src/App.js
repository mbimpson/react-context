import React, { useState } from 'react';
import ThemeContext from './context/ThemeContext';
import Header from './components/header';

function App() {
  
  const theme = useState('light');

  return (
    <ThemeContext.Provider value={theme}>
      <Header></Header>
    </ThemeContext.Provider>
  );
}

export default App;
