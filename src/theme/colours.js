const AppTheme = {
  light: {
      textColor: '#000',
      backgroundColor: '#9ED8F7'
  },
  dark: {
      textColor: '#fff',
      backgroundColor: 'gray'
  }
}

export default AppTheme;
